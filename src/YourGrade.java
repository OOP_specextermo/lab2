import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input your score: ");
        String strScore =  sc.next();
        int Score = Integer.parseInt(strScore);
        String grade = "";
        if(Score >= 0 && Score <= 100) {
            if(Score >=80){
                grade = "A"; 
            }
            else if (Score >=75) {
                grade = "B+";
            } 
            else if (Score >=70) {
                grade = "B";
            }
            else if (Score >=65) {
                grade = "C+";
            }
            else if (Score >=60) {
                grade = "C";
            }
            else if (Score >=55) {
                grade = "D+";
            }
            else if (Score >=50) {
                grade = "D";
            } 
            else{
                grade = "F";
            }
        }
        else {
            grade = "ERORR";
        }
    
        System.out.println("Grade: " +grade);
    }

}
